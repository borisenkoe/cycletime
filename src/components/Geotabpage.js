import React from 'react'
import moment from 'moment'
import DatePicker from 'material-ui/DatePicker';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import {Table, TableBody, TableHeader, TableHeaderColumn, TableRow, TableRowColumn} from 'material-ui/Table';
import TextField from 'material-ui/TextField';
import ReportRow from './ReportRow.js';
import DropDownMenu from 'material-ui/DropDownMenu';
import MenuItem from 'material-ui/MenuItem';
import RaisedButton from 'material-ui/RaisedButton';
import CircularProgress from 'material-ui/CircularProgress';
import RULENAMES from '../constants/constants'

class GeotabPage extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            rules: [],
            insideCutBlockId: '',
            fromDate: moment().startOf('day').format('YYYY-MM-DD'),
            toDate: moment().startOf('day').add(1, 'days').format('YYYY-MM-DD'),
            zones: [],
            routes: [],
            quantityOfDevices: 0,
            loadedDevices: 0,
            devicesWithRoutes: 0,
            loadedDevicesWithRoutes: 0,
            decimal: localStorage.getItem('decimal') || true,
            routeSearch: 'All',
            millSearch: 'All',
            contractorSearch: 'All',
            vehicleSearch: 'All',
            driveTimeSearch: 8,
            loadTimeSearch: 0,
            loadCheckSearch: 0,
            brakeCheckSearch: 0,
            disabledButton: false,
            nameSet: [],
            millSiteSet: [],
            contractorSet: [],
            vehicleSet: [],
            sets: [['All'], ['All'], ['All'], ['All']],
            version: 1,
            finished: false,
            expandAll: false,
            excelSummary: false,
            excelNoSummary: false
        }
    }

    getData = (groups) => {
        if (this.state.routes.length){
            this.setState({routes: []})
        }
        this.setState({disabledButton:true, finished: false});

        var calls = [
            ['Get', {typeName: 'Device', search: {groups: groups}}],
            ['Get', {typeName: 'Zone'}],
            ['Get', {typeName: 'Group'}]
        ];
        RULENAMES.forEach(ruleName => {
            calls.push(['Get', {typeName: 'Rule', search:{name: ruleName}}])
        });
        cycleTime.api.multiCall(calls)
            .then(results => {
                const devices = results[0];
                const receivedGroups = results[2];
                devices.forEach(device => {
                    device.groups.forEach(deviceGroup => {
                        receivedGroups.forEach(group => {
                            if (deviceGroup.id == group.id){
                                deviceGroup.name = group.name
                            }
                        })
                    })
                });
                console.log(results[1], 'ZONES');
                this.setState({zones: results[1]});
                let neededRules = [];
                for (let i = 3; i<results.length; i++){
                    if (results[i].length) neededRules.push(results[i][0])
                }
                this.setState({insideCutBlockId: results[3].id});

                this.onDevicesAndRules(devices, neededRules)
            })
    };

    onDevicesAndRules = (devices, rules) => {
        this.setState({quantityOfDevices:devices.length});
        let index = 0;
        let filteredDevices = [];
        const asyncLoop =  () => {
            console.log(index, "INDEX");
            this.setState({loadedDevices:index});
            if (index == devices.length) {
                console.log(filteredDevices, "FROM LOOP");
                this.onFilteredDevices(filteredDevices);
                return
            }
            let device = devices[index];
            let calls = [];
            rules.forEach(rule => {
                calls.push(['Get', {typeName: 'ExceptionEvent', search:{
                    deviceSearch: {id: device.id},
                    ruleSearch: {id: rule.id},
                    fromDate: new Date(this.state.fromDate).toISOString(),
                    toDate: new Date(this.state.toDate).toISOString()
                }}])
            });
            cycleTime.api.multiCall(calls)
                .then(results => {
                    if (results[0].length){
                        device.exceptions = [];
                        results.forEach((es, i) => {
                            es.forEach(e => {
                                e.rule.name = rules[i].name
                            });
                            device.exceptions.push(es)
                        });
                        filteredDevices.push(device);
                        index++;
                        asyncLoop();
                    } else {
                        index ++;
                        asyncLoop();
                    }
                })
        };
        asyncLoop();
    };

    onFilteredDevices = (devices) => {
        this.setState({devicesWithRoutes:devices.length});
        let index = 0;
        let devicesWithZones = [];
        const asyncLoop = () => {
            this.setState({loadedDevicesWithRoutes:index});
            console.log(index, "INDEX");
            if (index == devices.length) {
                console.log(devicesWithZones, "FROM SECOND LOOP");
                this.getRoutes(devicesWithZones);

                return
            }
            let device = devices[index];
            device.exceptions[1].forEach(e => device.exceptions[0].push(e));
            device.exceptions.splice(1,1);
            let calls = [];
            device.exceptions[0].forEach((e) => {
                let toDate = moment(e.activeFrom).add(1, 'minutes').toISOString();
                let query = {
                    typeName: 'LogRecord',
                    search: {
                        fromDate: e.activeFrom,
                        toDate: toDate,
                        deviceSearch: {
                            id: e.device.id
                        }
                    },
                    resultsLimit: 1
                };
                calls.push(["Get", query])
            });
            cycleTime.api.multiCall(calls)
                .then(logs => {
                    device.exceptions[0].forEach((e, i) => {
                        if (!logs[i].length) {
                            e.location = {
                                latitude: logs[i].latitude,
                                longitude: logs[i].longitude
                            };
                        } else {
                            e.location = {
                                latitude: logs[i][0].latitude,
                                longitude: logs[i][0].longitude
                            };
                        }
                    });
                    return device
                }).then(device => {
                let calls = [];
                device.exceptions[0].forEach(e => {
                    let query = {
                        coordinates: [{
                            x: e.location.longitude,
                            y: e.location.latitude
                        }]
                    };
                    calls.push(['GetAddresses', query])
                });
                cycleTime.api.multiCall(calls)
                    .then(addresses => {
                        if (addresses.length > 1) {
                            device.exceptions[0].forEach((e, i) => {
                                if (!addresses[i].length) {
                                    return;
                                }
                                e.zones = addresses[i][0].zones;
                                if (e.zones) {
                                    e.zones.forEach(zone => {
                                        this.state.zones.forEach(_zone => {
                                            if (_zone.id == zone.id) {
                                                zone.name = _zone.name
                                            }
                                        })
                                    })
                                }
                            })
                        } else {
                            device.exceptions[0].forEach((e) => {
                                e.zones = addresses[0].zones;
                                if (e.zones) {
                                    e.zones.forEach(zone => {
                                        this.state.zones.forEach(_zone => {
                                            if (_zone.id == zone.id) {
                                                zone.name = _zone.name
                                            }
                                        })
                                    })
                                }
                            })
                        }
                        if (device.exceptions[0].every(e => e.zones)) {
                            devicesWithZones.push(device);
                        }
                        index++;
                        asyncLoop();
                    });
            })
        };
        asyncLoop();
    };



    getRoutes(devices) {
        let routes = [];
        devices.forEach(device => {
            let exceptions = [];
            device.exceptions.forEach(es => {
                es.forEach(e => exceptions.push(e))
            });
            exceptions.sort((n1, n2) => new Date(n1.activeFrom) - new Date(n2.activeFrom));
            device.exceptions = exceptions;
            let route = {};
            let startIndex = null;
            let endIndex = null;
            for ( let i = 0; i<exceptions.length; i++){
                if (exceptions[i].rule.name == 'Inside Cut Block') {
                    if(startIndex && endIndex && exceptions[i].zones[0].name == startIndex.zoneName){
                        route.points = [];
                        route.points[0] = device.exceptions.slice(startIndex.i, endIndex + 1);
                        route.points[1] = device.exceptions.slice(endIndex, i + 1);
                        route.device = device;
                        route.full = true;
                        routes.pop();
                        if (route.points[0].length && route.points[1].length){
                            routes.push(route);
                        }
                        route = {};
                    }
                    startIndex = {i: i, zoneName: exceptions[i].zones[0].name};
                }
                if (exceptions[i].rule.name == 'Inside Mill Site' && startIndex) {
                    endIndex = i;
                    route.points = [];
                    route.points[0] = device.exceptions.slice(startIndex.i, endIndex + 1);
                    route.device = device;
                    route.full = false;
                    if (route.points[0].length){
                        routes.push(route);
                    }
                    route = {};
                }
            }
        });
        console.log(routes, 'ROUTES');
        this.prepareRoutesData(routes);
    }

    calculateDifference = (point) => {
        return moment(point.activeTo) - moment(point.activeFrom);
    };


    prepareRoutesData = (routes) => {

        let nameSet = new Set();
        let millSiteSet = new Set();
        let contractorSet = new Set();
        let vehicleSet = new Set();

        routes.forEach((route) => {
            for (let i = 1; i<route.points[0].length; i++){
                if (route.points[0][i].ruleName === RULENAMES[3] && this.calculateDifference(route.points[0][i]) > 600000){
                        route.points[0].splice(1,i);
        }
            }
            let millSite = route.points[0][route.points[0].length - 1].zones.filter(zone => {
                if (zone.name){
                  return  zone.name.indexOf('Mill') != -1
                }
            });
            if (!millSite[0]) {
                if (route.points[0][route.points[0].length - 1].zones[0] && route.points[0][route.points[0].length - 1].zones[0].name){
                    millSite = route.points[0][route.points[0].length - 1].zones[0].name.slice(5)
                } else {
                    millSite =  'No MillSite Zone';
                }
            } else {millSite = millSite[0].name.slice(5)}
            let cutBlock = route.points[0][0].zones.filter(zone => {
                if (zone.name) {
                    return zone.name.indexOf('CB') != -1
                }
            });
            if (!cutBlock[0]) {
                if (route.points[0][0].zones[0] && route.points[0][0].zones[0].name){
                    cutBlock = route.points[0][0].zones[0].name.slice(10)
                } else {
                    cutBlock = 'No CutBlock Zone';
                }
            } else {cutBlock = cutBlock[0].name.slice(10)}
            let routeName = cutBlock + ' - ' + millSite;
            route.routeName = routeName;
            nameSet.add(routeName);
            route.millSite = millSite;
            millSiteSet.add(millSite);
            let groups = route.device.groups.map(group => group.name);
            route.groups = groups.filter(group => {
                if (!group) return
               return group.indexOf('D-') != -1;
            });
            let group;
            if (!route.groups.length){
                group = ''
            }
            else {
                group = route.groups[0].slice(2);
            }
            route.group = group;
            contractorSet.add(group);
            let vehicle = route.device.name;
            route.vehicle = vehicle;
            vehicleSet.add(vehicle);
            const departureTime = moment(route.points[0][0].activeTo);
            const arrivalTime = moment(route.points[0][route.points[0].length -1].activeFrom);

            route.tripTime = arrivalTime - departureTime;
            route.timeInCutBlock = moment(route.points[0][0].activeTo) - moment(route.points[0][0].activeFrom);
            route.timeDriveInCutBlock = 0;
            route.timeStoppedInCutBlock = 0;
            route.brakeOccurances = 0;
            route.timeBrakeCheck = 0;
            route.wrapperCheckOccurances = 0;
            route.timeWrapperCheck = 0;
            route.timeOutsideStops = 0;
            route.points[0].forEach(point => {
                let ruleName = point.rule.name;
                if (ruleName === RULENAMES[2]){
                    route.timeDriveInCutBlock += this.calculateDifference(point);
                }
                if (ruleName === RULENAMES[3]){
                    route.timeStoppedInCutBlock += this.calculateDifference(point);
                }
                if (ruleName === RULENAMES[4] || ruleName === RULENAMES[5]){
                    route.brakeOccurances++;
                    route.timeBrakeCheck += this.calculateDifference(point);
                }
                if (ruleName === RULENAMES[6] || ruleName === RULENAMES[7]){
                    route.wrapperCheckOccurances++;
                    route.timeWrapperCheck += this.calculateDifference(point);
                }
                if (ruleName === RULENAMES[8]){
                    route.timeOutsideStops += this.calculateDifference(point);
                }
            });
            route.driveTime = arrivalTime - departureTime - route.timeBrakeCheck - route.timeWrapperCheck - route.timeOutsideStops;
            route.millSiteTime = moment(route.points[0][route.points[0].length -1].activeTo) - moment(route.points[0][route.points[0].length -1].activeFrom);
            route.cycleTimeHalf = route.driveTime + route.millSiteTime;
            if (route.points.length === 2){
                route.returnStopOccurances = 0;
                route.returnStopTime = 0;
                route.points[1].forEach(point => {
                    let ruleName = point.rule.name;
                    if (ruleName != RULENAMES[0] && ruleName != RULENAMES[1] && ruleName != RULENAMES[2]){
                        route.returnStopOccurances++;
                        route.returnStopTime += this.calculateDifference(point);
                    }
                });
                route.returnDriveTime = moment(route.points[1][route.points[1].length-1].activeFrom) - moment(route.points[1][0].activeTo) - route.returnStopTime;
                route.cycleTimeFull = route.cycleTimeHalf + route.returnDriveTime;
            }
        });

        nameSet = [...nameSet];
        millSiteSet = [...millSiteSet];
        contractorSet = [...contractorSet];
        vehicleSet = [...vehicleSet];
        let groupedRoutes = [];
        nameSet.forEach((routeName, i) => {
            groupedRoutes[i] = [];
            routes.forEach(route => {
                if (routeName === route.routeName) groupedRoutes[i].push(route);
            })
        });
        groupedRoutes = groupedRoutes.filter(routes => routes.length);
        this.setState({routes: groupedRoutes, sets: [nameSet, millSiteSet, contractorSet,vehicleSet], disabledButton:false,
            quantityOfDevices: 0, loadedDevices: 0, devicesWithRoutes: 0, loadedDevicesWithRoutes: 0, finished: true})
    };

    toggleDecimal = () => {
        localStorage.setItem('decimal', !this.state.decimal);
        this.setState({decimal:!this.state.decimal})
    };

    filterDeepGroups = (routes) => {
        routes.forEach((routess,i) => {
            routes[i] = routess.filter(route => route.group.toLowerCase().indexOf(this.state.contractorSearch.toLowerCase()) !== -1)
        });
        return routes
    };

    filterDeepVehicles = (routes) => {
        routes.forEach((routess,i) => {
            routes[i] = routess.filter(route => route.device.name.toLowerCase().indexOf(this.state.vehicleSearch.toLowerCase()) !== -1)
        });
        return routes
    };

    filterDeepTimeLess = (routes, options) => {
           routes.forEach((routess,i) => {
            routes[i] = routess.filter(route => {
                if (options.time === 'returnDriveTime' && !route.full) return true;
                return route[options.time] < this.state[options.search]*options.mills
            })
           });
        return routes
    };

    filterDeepTimeMore = (routes, options) => {
        routes.forEach((routess,i) => {
            routes[i] = routess.filter(route => route[options.time] > this.state[options.search]*options.mills)
        });
        return routes
    };

    filterTable = (routes) => {
        let filteredRoutes = this.state.routeSearch !== 'All' ? routes.filter(routes => {
            return routes[0].routeName.toLowerCase().indexOf(this.state.routeSearch.toLowerCase()) !== -1
        }) : routes;
        filteredRoutes = this.state.millSearch !== 'All' ? filteredRoutes.filter(routes => {
            return routes[0].millSite.toLowerCase().indexOf(this.state.millSearch.toLowerCase()) !== -1
        }) : filteredRoutes;

        filteredRoutes = this.state.contractorSearch !== 'All' ? this.filterDeepGroups(filteredRoutes)
         : filteredRoutes;

        filteredRoutes = this.state.vehicleSearch !== 'All' ? this.filterDeepVehicles(filteredRoutes)
            : filteredRoutes;

        filteredRoutes = +this.state.driveTimeSearch ? this.filterDeepTimeLess(filteredRoutes, {
            time: 'driveTime', search: 'driveTimeSearch', mills: 3600000
        }) : filteredRoutes;
        filteredRoutes = +this.state.driveTimeSearch ? this.filterDeepTimeLess(filteredRoutes, {
            time: 'returnDriveTime', search: 'driveTimeSearch', mills: 3600000
        }) : filteredRoutes;
        filteredRoutes = +this.state.loadTimeSearch ? this.filterDeepTimeMore(filteredRoutes, {
            time: 'timeInCutBlock', search: 'loadTimeSearch', mills: 60000
        }) : filteredRoutes;
        filteredRoutes = +this.state.loadCheckSearch ? this.filterDeepTimeLess(filteredRoutes, {
            time: 'timeWrapperCheck', search: 'loadCheckSearch', mills: 60000
        }) : filteredRoutes;
        filteredRoutes = +this.state.brakeCheckSearch ? this.filterDeepTimeLess(filteredRoutes, {
            time: 'timeBrakeCheck', search: 'brakeCheckSearch', mills: 60000
        }) : filteredRoutes;

        return filteredRoutes ?  filteredRoutes.filter(routes => routes.length) : [];
    };

    refreshSets = (routes) => {
        let nameSet = new Set();
        let millSiteSet = new Set();
        let contractorSet = new Set();
        let vehicleSet = new Set();
        routes.forEach(routes => {
            routes.forEach(route => {
                nameSet.add(route.routeName);
                millSiteSet.add(route.millSite);
                contractorSet.add(route.group);
                vehicleSet.add(route.device.name);
            })
        });
        const handleSet = (set) => {
            if (set.size) set = [...set];
            else set = [];
            set.unshift('All');
            return set
        };
        nameSet = handleSet(nameSet);
        millSiteSet = handleSet(millSiteSet);
        contractorSet = handleSet(contractorSet);
        vehicleSet = handleSet(vehicleSet);
        return [nameSet, millSiteSet, contractorSet,vehicleSet]

    };

    handleChangeMinDate = (value) => {
        this.setState({fromDate: value});
        this.setState({toDate: moment(value).add(1, 'days').format('YYYY-MM-DD')});
        this.setState({version: 1})
    };

    handleChangeMaxDate = (value) => {
        this.setState({toDate: value});
        this.setState({version: 1})
    };

    handleTime  = (e, key) => {
        if (e.target.value >= 0) {
            this.setState({[key]: e.target.value})
        }
    };

    createTable = () => {
        $("#table").table2excel({
            exclude: ".noExl",
            name: "Cycle Time",
            fileName: `cycle-time-v${this.state.version}-${this.state.fromDate}-${this.state.toDate}`
        });
        this.setState({version: ++this.state.version, excelSummary:false, excelNoSummary: false})

    };

    resultsToTableSummary = () => {
        this.setState({excelSummary:true, expandAll: true});
        let promise = new Promise((resolve) => {
            setInterval(() => {
                if (this.state.excelSummary && this.state.expandAll){
                    resolve();
                }
            }, 500)

        });

        promise
            .then(result => this.createTable());

    };

    resultsToTableNoSummary = () => {
        this.setState({excelNoSummary:true, expandAll: true});
        let promise = new Promise((resolve) => {
            setInterval(() => {
                if (this.state.excelNoSummary && this.state.expandAll){
                    resolve();
                }
            }, 500)

        });

        promise
            .then(result => this.createTable());

    };


    render() {
        const groups = this.props.groups;
        const headerNames = ['Time in Cut Block', 'Drive Time in Cut Block', 'Time Stopped in Cut Block',
        'Brake Check Occurances', 'Brake Check Time', 'Wrapper Check Occurances', 'Wrapper Check Time',
        'Unscheduled Stop Time', 'Loaded Drive Time', 'Cumulative Time at Mill Site', 'Return Trips',
        'Return Stop Occurances', 'Cumulative Stops on Return Time', 'Return To Block Drive Time'];
        let deviceString = `Loaded ${this.state.loadedDevices} of ${this.state.quantityOfDevices} devices`;
        let routeString = `Loaded data for ${this.state.loadedDevicesWithRoutes} of ${this.state.devicesWithRoutes} devices with routes`;
        let buttonText = this.state.decimal ? "Switch to hours" : "Switch to decimal";
        let routes = [...this.state.routes];
        let filteredRoutes = this.filterTable(routes);
        let sets = this.refreshSets(filteredRoutes);
        let filterStyle = {height: '30px', width: '50px'};
        let spanStyle = {paddingLeft: '10px'};
        let dropStyle = {whiteSpace: 'nowrap', overflow: 'hidden'};
        let buttonStyle = {marginLeft:'10px', marginTop: '5px'};
        let isExcel = this.state.excelSummary || this.state.excelNoSummary;
        let excelItemSummary = <div style = {{display: 'inline-block', paddingLeft: '20px', position: 'absolute'}}>
            <p style = {{margin: '1px'}}>Export with summary</p><img src = 'https://dl.dropboxusercontent.com/u/551937608/cycletime/excel.png'
                             style = {{marginLeft: '3.5em', height:'30px',  cursor: 'pointer'}}
                             onClick= {() => this.resultsToTableSummary()}/>
                        </div>;
        let excelItemNoSummary = <div style = {{display: 'inline-block', marginLeft: '15em', position: 'absolute'}}>
            <p style = {{margin: '1px'}}>Export without summary</p><img src = 'https://dl.dropboxusercontent.com/u/551937608/cycletime/excel.png'
                                                                     style = {{marginLeft: '3.5em', height:'30px',  cursor: 'pointer'}}
                                                                     onClick= {() => this.resultsToTableNoSummary()}/>
        </div>;
        return (
            <MuiThemeProvider>
                <div style= {{overflow:'auto', height: '100%'}}>
                    <div style ={{position:'relative'}}>
                    <input type="date" value = {this.state.fromDate}
                           onChange={(e) => {e.preventDefault();
                                             const target = (e.currentTarget) ? e.currentTarget : e.srcElement;
                                             this.handleChangeMinDate(target.value)

                                        }
                           }
                    />
                    <input type="date" value = {this.state.toDate} min = {this.state.fromDate}
                           onChange={(e) => {e.preventDefault();
                                             const target = (e.currentTarget) ? e.currentTarget : e.srcElement;
                                             this.handleChangeMaxDate(target.value)
                                        }
                           }
                    />

                    <RaisedButton style = {buttonStyle} disabled = {this.state.disabledButton} label = 'Create Report'
                                  onClick = {() => this.getData(groups)}/>
                    <RaisedButton style = {buttonStyle} onClick = {this.toggleDecimal} label = {buttonText} />
                        {excelItemSummary}
                        {excelItemNoSummary}
                        </div>
                    <div style = {{marginTop: '10px'}}>
                        <span>Filter:</span>
                        <span style = {spanStyle}>Drive Time <TextField type = 'number' value = {this.state.driveTimeSearch}
                                                    style = {filterStyle} onChange={(e)=> this.handleTime(e, 'driveTimeSearch')} /> (hrs) </span>
                        <span style = {spanStyle}>Min Load Time <TextField type = 'number' value = {this.state.loadTimeSearch}
                                                                           style = {filterStyle}
                                                                           onChange={(e)=> this.handleTime(e, 'loadTimeSearch')} />
                         (mins) </span>
                        <span style = {spanStyle}>Load Check Max <TextField type = 'number' value = {this.state.loadCheckSearch}
                                                               style = {filterStyle}
                                                               onChange={(e)=> this.handleTime(e, 'loadCheckSearch')} /> (mins) </span>
                        <span style = {spanStyle}>Brake Check Max <TextField type = 'number' value = {this.state.brakeCheckSearch}
                                                                style = {filterStyle}
                                                                onChange={(e)=> this.handleTime(e, 'brakeCheckSearch')} /> (mins) </span>
                    </div>
                    {this.state.disabledButton && <div style = {{display:'block', fontSize: '14px', fontWeight: 'bold'}}>
                        <div style = {{display: 'inline-block'}}><CircularProgress mode='indeterminate' size={0.4}/></div>
                        <div style = {{display: 'inline-block'}}><span style = {{color:'red'}}>  Creating the report</span><br/>
                        {deviceString}<br/>
                        {routeString}<br/>
                    </div>
                    </div>}
                    <div>
                        <table id = "table" style = {isExcel ? {display: 'none'}: {display:'table'}}>
                            <thead>
                                <tr>
                                    {!this.state.excelNoSummary && <th> Route    <span style = {{fontWeight:'bold', cursor:'pointer'}}
                                                        onClick = {() => this.setState({expandAll:!this.state.expandAll})}
                                    >  {this.state.expandAll ? '--' : '++'}  </span>
                                        {(!this.state.excelSummary || !this.state.excelNoSummary)
                                        && <DropDownMenu  maxHeight = {400} style = {dropStyle}
                                                      value = {this.state.routeSearch}
                                                      onChange={(e,i,value) => this.setState({routeSearch:value})}>
                                            {sets[0].map((name, i) => {
                                                return  <MenuItem key={i} value={name} primaryText={name}/>
                                            })
                                            }
                                        </DropDownMenu>}
                                    </th>}
                                    {this.state.excelNoSummary ? <th>Routes</th> : <th/>}
                                    <th> MillSite
                                        {isExcel ? null : <DropDownMenu  maxHeight = {400} style = {dropStyle}
                                                      value = {this.state.millSearch}
                                                      onChange={(e,i,value) => this.setState({millSearch:value})}>
                                            {sets[1].map((name, i) => {
                                                return  <MenuItem key={i} value={name} primaryText={name}/>
                                            })
                                            }
                                        </DropDownMenu>}
                                    </th>
                                    <th> Contractor
                                        {isExcel ? null : <DropDownMenu  maxHeight = {400} style = {dropStyle}
                                                      value = {this.state.contractorSearch}
                                                      onChange={(e,i,value) => this.setState({contractorSearch:value})}>
                                            {sets[2].map((name, i) => {
                                                return  <MenuItem key={i} value={name} primaryText={name}/>
                                            })
                                            }
                                        </DropDownMenu>}
                                    </th>
                                    <th> Truck/Vehicle
                                        {isExcel ? null : <DropDownMenu  maxHeight = {400} style = {dropStyle}
                                                      value = {this.state.vehicleSearch}
                                                      onChange={(e,i,value) => this.setState({vehicleSearch:value})}>
                                            {sets[3].map((name, i) => {
                                                return  <MenuItem key={i} value={name} primaryText={name}/>
                                            })
                                            }
                                        </DropDownMenu>}
                                    </th>
                                    <th> Event Time </th>
                                    {!this.state.excelNoSummary && <th> Trips to Mill Site </th>}
                                    {headerNames.map(header => <th>{header}</th>)}
                                </tr>
                            </thead>

                            {filteredRoutes.length ? filteredRoutes.map((routes, i) => {
                                return <ReportRow key = {i} routes = {routes} decimal = {this.state.decimal}
                                 expandAll = {this.state.expandAll} excelSummary = {this.state.excelSummary}
                                                  excelNoSummary = {this.state.excelNoSummary}/>

                            }) : this.state.finished ? "No routes matches the filters " : null}


                        </table>

                  </div>
                </div>
            </MuiThemeProvider>
        )
    }
}
export default GeotabPage;



//let route = {};
//let startIndex = null;
//let endIndex = null;
//for ( let i = 0; i<exceptions.length; i++){
//    if (exceptions[i].rule.name == 'Inside Cut Block') {
//        startIndex = i;
//    }
//    if (exceptions[i].rule.name == 'Inside Mill Site' && startIndex !== null) {
//        endIndex = i;
//        route.points = device.exceptions.slice(startIndex, endIndex + 1);
//        route.device = device;
//        routes.push(route);
//
//        route = {};
//        startIndex = null;
//        endIndex = null;
//    }
//
//}
