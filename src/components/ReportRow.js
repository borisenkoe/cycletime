import React from 'react';
import moment from 'moment';
import ExpandedRow from './ExpandedRow.js';
import {Table, TableBody, TableHeader, TableHeaderColumn, TableRow, TableRowColumn} from 'material-ui/Table';

class ReportRow extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            expanded: false
        }
    }

    formatTimeFromMills = (mills) => {
        if (mills === '') return '';
        if (this.props.decimal){
            return (mills/3600000).toFixed(1);
        } else {
            let hours = Math.floor(mills/3600000);
            let leftMills = mills%3600000;
            let minutes = Math.floor(leftMills/60000);
            if (minutes<10){
                minutes = '0' + minutes
            }
            return hours + ':' + minutes;
        }
    }

    calculateAverage = (key) => {
        const routes = this.props.routes;
        let result = routes.reduce((sum, route) => {
            return sum + route[key];
        }, 0);
        return (result/routes.length).toFixed(0);
    }

    calculateAverageReturn = (key) => {
        const routes = this.props.routes.filter(route => route.full);
        if (!routes.length) return '';
        let result = routes.reduce((sum, route) => {
            return sum + route[key];
        }, 0);
        return (result/routes.length).toFixed(0);
    }

    openLiveLocation = (route) => {
        window.open(`#map,liveVehicleIds:!(${route.device.id})`)
    }

    openTrip = (route) => {
        let query = [
            'tripsHistory',
            'dateRange:(startDate:\'' + route.points[0][0].activeTo + '\',endDate:\'' + route.points[0][route.points[0].length -1].activeFrom + '\')',
            'devices:!(' + route.device.id + ')'
        ].join(',');
        window.open('#' + query);
    }

    componentWillReceiveProps (newProps) {
        if (newProps.expandAll != this.props.expandAll){
            if (newProps.expandAll != this.state.expanded){
                this.setState({expanded:!this.state.expanded})
            }
        }
    }

    calculateReturnTrips(){
      return this.props.routes.reduce((sumOfTrue,route) => route.full ? sumOfTrue + 1 : sumOfTrue,0)
    }

    render(){
        const routes = this.props.routes;

        return(
            <tbody>
            {!this.props.excelNoSummary && <tr>
                <td style={{whiteSpace:'normal'}}
                    >
                    <span onClick={() => this.openTrip(routes[0])}
                          style={{cursor:'pointer', color:'blue',textDecoration:'underline'}}>{routes[0].routeName}</span>
                    <span style = {{fontWeight:'bold', cursor:'pointer'}}
                          onClick = {() => this.setState({expanded:!this.state.expanded})}
                    >  {this.state.expanded ? '-' : '+'}  </span>
                </td>
                <td/>
                <td>{routes[0].millSite}</td>
                <td> </td>
                <td style={{color:'blue',textDecoration:'underline',textAlign:'center'}}
                    > </td>
                <td> </td>
                <td>{routes.length}</td>
                <td>{this.formatTimeFromMills(this.calculateAverage('timeInCutBlock'))}</td>
                <td>{this.formatTimeFromMills(this.calculateAverage('timeDriveInCutBlock'))}</td>
                <td>{this.formatTimeFromMills(this.calculateAverage('timeStoppedInCutBlock'))}</td>
                <td>{this.calculateAverage('brakeOccurances')}</td>
                <td>{this.formatTimeFromMills(this.calculateAverage('timeBrakeCheck'))}</td>
                <td>{this.calculateAverage('wrapperCheckOccurances')}</td>
                <td>{this.formatTimeFromMills(this.calculateAverage('timeWrapperCheck'))}</td>
                <td>{this.formatTimeFromMills(this.calculateAverage('timeOutsideStops'))}</td>
                <td>{this.formatTimeFromMills(this.calculateAverage('driveTime'))}</td>
                <td>{this.formatTimeFromMills(this.calculateAverage('millSiteTime'))}</td>
                <td>{this.calculateReturnTrips()}</td>
                <td>{this.calculateAverageReturn('returnStopOccurances')}</td>
                <td>{this.formatTimeFromMills(this.calculateAverageReturn('returnStopTime'))}</td>
                <td>{this.formatTimeFromMills(this.calculateAverageReturn('returnDriveTime'))}</td>

            </tr>}
            {this.state.expanded ? routes.map((route, i)=> {
                return <ExpandedRow key = {i} route = {route} decimal = {this.props.decimal} openTrip = {this.openTrip}
                                    openLiveLocation = {this.openLiveLocation} formatTimeFromMills = {this.formatTimeFromMills}
                                    excelSummary = {this.props.excelSummary} excelNoSummary = {this.props.excelNoSummary}/>
            }) : null}
        </tbody>
        )
    }
}

export default ReportRow
//<TableRowColumn>{routeName}</TableRowColumn>
//<TableRowColumn>{route.points[route.points.length - 1].zones[0]}</TableRowColumn>
//<TableRowColumn>{route.device.groups[0]}</TableRowColumn>
//<TableRowColumn>{route.device.name}</TableRowColumn>
//<TableRowColumn>{moment(route.points[0].activeTo) - moment(route.points[route.points.length - 1].activeFrom)}</TableRowColumn>
