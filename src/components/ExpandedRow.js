import React from 'react';
import moment from 'moment';
import {Table, TableBody, TableHeader, TableHeaderColumn, TableRow, TableRowColumn} from 'material-ui/Table';

class ExpandedRow extends React.Component {
    constructor(props) {
        super(props);
        this.state = {

        }
    }

    formatTimeFromMills = (mills) => {
        if (this.props.decimal){
            return (mills/3600000).toFixed(1);
        } else {
            let hours = Math.floor(mills/3600000);
            let leftMills = mills%3600000;
            let minutes = Math.floor(leftMills/60000);
            if (minutes<10){
                minutes = '0' + minutes
            }
            return hours + ':' + minutes
        }
    };

    render(){
        const route = this.props.route;
        const formatTimeFromMills = this.props.formatTimeFromMills;
        return(
            <tr>
                {!this.props.excelNoSummary && <td/>}
                <td style={{color:'blue',textDecoration:'underline',textAlign:'center', whiteSpace:'normal'}}
                                onClick={() => this.props.openTrip(route)}>
                    <span style={{cursor:'pointer'}}>{route.routeName}</span></td>
                <td>{route.millSite}</td>
                <td>{route.group}</td>
                <td style={{color:'blue',textDecoration:'underline',textAlign:'center'}}
                                onClick={() => this.props.openLiveLocation(route)}>
                    <span style={{cursor:'pointer'}}>{route.vehicle}</span></td>
                <td>{moment.utc(route.points[0][0].activeTo).format('MM-DD-YYYY HH:mm')}</td>
                {!this.props.excelNoSummary && <td> </td>}
                <td>{formatTimeFromMills(route.timeInCutBlock)}</td>
                <td>{formatTimeFromMills(route.timeDriveInCutBlock)}</td>
                <td>{formatTimeFromMills(route.timeStoppedInCutBlock)}</td>
                <td>{route.brakeOccurances}</td>
                <td>{formatTimeFromMills(route.timeBrakeCheck)}</td>
                <td>{route.wrapperCheckOccurances}</td>
                <td>{formatTimeFromMills(route.timeWrapperCheck)}</td>
                <td>{formatTimeFromMills(route.timeOutsideStops)}</td>
                <td>{formatTimeFromMills(route.driveTime)}</td>
                <td>{formatTimeFromMills(route.millSiteTime)}</td>
                <td>{route.full ? 1 : 0}</td>
                <td>{route.returnStopOccurances ? route.returnStopOccurances : route.full ? 0 : ''}</td>
                <td>{route.returnStopTime ? formatTimeFromMills(route.returnStopTime) : ''}</td>
                <td>{route.returnDriveTime ? formatTimeFromMills(route.returnDriveTime) : ''}</td>
            </tr>
        )
    }
}

export default ExpandedRow;