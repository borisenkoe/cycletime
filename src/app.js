import React from 'react'
import {render, unmountComponentAtNode} from 'react-dom'
import GeotabPage from './components/GeotabPage.js'
import injectTapEventPlugin from 'react-tap-event-plugin';
window.cycleTime = {};
window.geotab.addin.cycleTime = function (api, state) {
    let cycleTime = window.cycleTime;
    cycleTime.api = api;
    cycleTime.state = state;
    return {
        initialize: function (api, state, callback) {
            //Needed for onTouchTap
            //Can go away when react 1.0 release
            injectTapEventPlugin();

            cycleTime.element = document.getElementById('cycletime');
            api.getSession(cred => {
                cycleTime.cred = cred;
                console.log(cred);
                cycleTime.main = render(<GeotabPage />, cycleTime.element, callback);
            })

        },
        focus: function () {
            api.getSession(cred => {
                cycleTime.cred = cred;
                let groups = state.getGroupFilter();
                console.log("focus called", groups);
                cycleTime.main = render(<GeotabPage groups={groups}/>, cycleTime.element);

            })
        },
        blur: function () {
            unmountComponentAtNode(cycleTime.element);
        }
    };
};












