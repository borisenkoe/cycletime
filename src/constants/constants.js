const RULENAMES = ['Inside Cut Block', 'Inside Mill Site', 'Drive In Cutblock', 'Stopped In Cutblock',
    'Inside Brake Check 2', 'Inside Brake Check 1 for CT', 'Inside Load Check 2', 'Inside Load Check for CT',
    'Stop Outside C/B/ M/S B/C W/C'];

export default RULENAMES;
